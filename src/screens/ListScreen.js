import React from 'react';
import {View, Text, StyleSheet,FlatList} from 'react-native';

const ListScreen = () =>{
    const sifu =[
        //key need to be string and unique
        //can use key Extractor to replace
        // {name: 'sifu #1',key: '1'},
        {name: 'sifu #1',age:20},
        {name: 'sifu #2',age:21},
        {name: 'sifu #3',age:22},
        {name: 'sifu #4',age:23},
        {name: 'sifu #5',age:24},
        {name: 'sifu #6',age:20},
        {name: 'sifu #7',age:26},
        {name: 'sifu #8',age:33},
        {name: 'sifu #9',age:21},
        {name: 'sifu #10',age:34}
    ];
    return (
        <FlatList
            // horizontal={true}   //show list horizontally
            // showsHorizontalScrollIndicator={false}  //dont show horizontal scrollbar
            keyExtractor={(sifu_key)=>sifu_key.name}
            data={sifu}
            // renderItem={(element)=>{}}
            // element ==> {item:{name: 'sifu #1'}, index:0}
            renderItem={({item})=>{
                return (
                    <Text style={styles.textStyle}>
                    {item.name} - Age {item.age}
                </Text>
                );
            }}
    
        />
    );
};

const styles = StyleSheet.create({
    textStyle:{
        marginVertical:50
    }
});

export default ListScreen;
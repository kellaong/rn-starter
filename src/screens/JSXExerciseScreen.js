//1. import library
import React from 'react';  //import entire react library
import {Text,StyleSheet, View } from 'react-native';  //import text and styesheet from library


//2. create component
//group text using view,cannot write comment in view tag
const ComponentsScreen = ()=>{  //()=>{} same as function (){}

    const subTopic = <Text style={styles.subTopicStyle}>My name is Kella ^^</Text>

   return <View>
        <Text style={styles.textStyle}>Getting started with react native!</Text>
        {subTopic}
        </View>
};


//3. add style
const styles = StyleSheet.create({
    textStyle:{
        fontSize:45
    },
    subTopicStyle:{
        fontSize:20
    }
});


//4.export
export default ComponentsScreen;